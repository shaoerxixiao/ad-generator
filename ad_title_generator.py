import jieba.posseg as pseg
from gensim.models import KeyedVectors,word2vec
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
import random
import heapq
import copy
import re

important_flags = {"n", "ns", "nz", "v", "vd", "vn", "vf", "vx", "vi", "vg", "vl", "a", "ad", "an", "ag", "al"}
stable_flags = {"ns", "x", "nt", "m", "b", "f", "nz", "uj"}
stable_words = {"碧桂园", "凤凰", "凤凰城", "五百强", "千亿", "您", "公园", "恭候", "天樾"}
words_unused = {'；', "。", "㎡", "这个", "那个", "了", "的", "地", "在","(", ")",}

ad_w2v_file = "data/ad_w2v.model"
tencent_w2v_file = "data/tac.txt"


class TitleGenerator:
    def __init__(self, input, data_file = "data/all_taglines_20.txt",topK=40,times=6):
        titles = []
        with open(data_file, "r", encoding="utf8")as fi:
            for title in fi:
                titles.append(title.strip())
        tencent_w2v_model = KeyedVectors.load_word2vec_format(tencent_w2v_file, binary=False, unicode_errors='ignore')
        ad_w2v_model = word2vec.Word2Vec.load(ad_w2v_file)

        self.input_sen = " ".join(input)
        self.titles = titles
        self.tencent_w2v_model = tencent_w2v_model
        self.ad_w2v_model = ad_w2v_model
        self.topK = topK
        self.times = times
        self.candidates = self.get_title_candidate()

    def get_title_candidate(self):
        # candidates = [t for w in self.key_words for t in self.titles if w in t]
        # titles_vecs = {(i,self.sen2vec(s)) for i, s in enumerate(self.titles)}
        key_words_vec = self.sen2vec(self.input_sen)
        sim_scores = []
        for sen in self.titles:
            sim_scores.append(cosine_similarity([self.sen2vec(sen)],
                                          [key_words_vec])[0, 0])
        candidates_index = list(map(sim_scores.index, heapq.nlargest(self.topK,sim_scores)))
        candidates = [self.titles[i] for i in candidates_index]
        #print(len(candidates))
        #print(candidates[-1])
        return candidates

    def generate_titles(self):
        ret = {}
        for t in self.candidates:
            ret[t] = []
            for _ in range(self.times):
                ret[t].append(self.generate_one_title(t))
        return ret

    def generate_one_title(self, title):
        title_generated = ""
        wf = pseg.cut(title)
        for w, f in wf:
            #print(w, f)
            if w in stable_words:
                title_generated += w
            elif f not in stable_flags:
                try:
                    tops = self.tencent_w2v_model.wv.similar_by_word(word=w, topn=8)
                    tops = [x[0] for x in tops]
                    tops_ = copy.deepcopy(tops)
                    for t in tops:
                        for w_un in words_unused:
                            if w_un in t:
                                tops_.remove(t)
                        if w in t:
                            tops_.remove(t)
                        if re.findall("\d+", t):
                            tops_.remove(t)
                    #print("tops_:", tops_)
                    if len(tops_) == 0:
                        title_generated += w
                    else:
                        candidate = random.choice(tops_)
                        #print(candidate)
                        title_generated += candidate
                except:
                    try:
                        top2 = self.ad_w2v_model.wv.similar_by_word(word=w, topn=2)
                        top5 = [x[0] for x in top5]
                        #print(top2)
                        candidate = random.choice(top2)
                        #print(candidate)
                        title_generated += candidate
                    except:
                        title_generated += w
            else:
                title_generated += w
        return title_generated

    def sen2vec(self, sen):
        sen_words = [w for w, f in pseg.cut(sen) if f in important_flags]
        w_vecs = []
        n = 0
        for w in sen_words:
            try:
                w_vec = self.tencent_w2v_model[w]
                n += 1
                w_vecs.append(w_vec)
            except:
                pass
        if n == 0:
            return np.array([0]*200)
        else:
            sen_vec = np.sum(w_vecs, 0)/n
            return sen_vec
